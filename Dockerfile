FROM openjdk:11
ADD target/users-service.jar users-service.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "users-service.jar"]